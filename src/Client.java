import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) throws RemoteException, NotBoundException, MalformedURLException,
			FileNotFoundException, NoSuchAlgorithmException {
		Scanner scanner = new Scanner(System.in);
		Service service = connect();
		boolean running = true;

		while (running) {
			System.out.println("\n");
			System.out.println("Print Server 19");
			System.out.println("Select function by inputting the function's number.\n");
			printFunctions(service.isOn());

			String selectionStr = scanner.nextLine();
			int selection;
			try {
				selection = Integer.parseInt(selectionStr);
			} catch (NumberFormatException e) {
				System.out.println("\nInvalid number.");
				continue;
			}

			System.out.println("\n--- " + handleSelection(selection, service, scanner));
		}

		scanner.close();
	}

	public static Service connect() throws MalformedURLException, RemoteException, NotBoundException {
		return (Service) Naming.lookup("rmi://localhost:5096/hello");
	}

	// Prints possible functions according to the server's status
	public static void printFunctions(boolean serverOn) throws RemoteException {
		if (serverOn) {
			System.out.println("1. print");
			System.out.println("2. queue");
			System.out.println("3. topQueue");
			System.out.println("4. status");
			System.out.println("5. readConfig");
			System.out.println("6. setConfig");
			System.out.println("7. restart");
			System.out.println("8. stop");
		} else {
			System.out.println("1. start");
		}
	}

	public static String handleSelection(int selection, Service service, Scanner scanner)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		System.out.println("");
		System.out.println("Input your username and password (separated by \"enter\").");
		String username = scanner.nextLine();
		String password = scanner.nextLine();

		System.out.println("");

		if (service.isOn()) {
			switch (selection) {
				case 1:
					System.out.println("Input file name and printer name (separated by \"enter\").");
					return service.print(username, password, scanner.nextLine(), scanner.nextLine());
				case 2:
					System.out.println("Input printer name.");
					return service.queue(username, password, scanner.nextLine());
				case 3:
					System.out.println("Input printer name and job number (separated by \"enter\").");
					return service.topQueue(username, password, scanner.nextLine(), scanner.nextInt());
				case 4:
					System.out.println("Input printer name.");
					return service.status(username, password, scanner.nextLine());
				case 5:
					System.out.println("Input parameter name.");
					return service.readConfig(username, password, scanner.nextLine());
				case 6:
					System.out.println("Input parameter name and the new value for the parameter (separated by \"enter\").");
					return service.setConfig(username, password, scanner.nextLine(), scanner.nextLine());
				case 7:
					return service.restart(username, password);
				case 8:
					return service.stop(username, password);
				default:
					return "Couldn't recognise that function. Please input the number for the desired function.";
			}
		} else {
			switch (selection) {
				case 1:
					return service.start(username, password);
				default:
					return "Couldn't recognise that function. Please input the number for the desired function.";
			}
		}
	}

	// Runs all print server functions
	public static void runAll(Service service) throws RemoteException, NotBoundException,
			FileNotFoundException, NoSuchAlgorithmException {
		String username = "alice";
		String password = "1234";

		System.out.println("--- " + service.start(username, password));
		System.out.println("--- " + service.status(username, password, "HP2700"));
		System.out.println("--- " + service.print(username, password, "test.txt", "HP2700"));
		System.out.println("--- " + service.status(username, password, "HP2700"));
		System.out.println("--- " + service.print(username, password, "test2.txt", "HP2700"));
		System.out.println("--- " + service.queue(username, password, "HP2700"));
		System.out.println("--- " + service.topQueue(username, password, "HP2700", 2));
		System.out.println("--- " + service.queue(username, password, "HP2700"));
		System.out.println("--- " + service.restart(username, password));
		System.out.println("--- " + service.queue(username, password, "HP2700"));
		System.out.println("--- " + service.readConfig(username, password, "color setting"));
		System.out.println("--- " + service.setConfig(username, password, "color setting", "black"));
		System.out.println("--- " + service.readConfig(username, password, "color setting"));
		System.out.println("--- " + service.stop(username, password));
	}
}