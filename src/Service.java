import java.io.FileNotFoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

public interface Service extends Remote {
	public HashMap<String, ArrayList<String>> tasks = new HashMap<String, ArrayList<String>>();
	public HashMap<String, String> config = new HashMap<>();

	// returns whether the server is on or not
	public boolean isOn() throws RemoteException;

	// authenticates a user
	public String authenticate(String username, String password, String permission)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// prints file filename on the specified printer
	public String print(String username, String password, String filename, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// lists the print queue for a given printer on the user's display in lines of
	// the form <job number> <file name>
	public String queue(String username, String password, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// moves job to the top of the queue
	public String topQueue(String username, String password, String printer, int job)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// starts the print server
	public String start(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// stops the print server
	public String stop(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// stops the print server, clears the print queue and starts the print server
	// again
	public String restart(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// prints status of printer on the user's display
	public String status(String username, String password, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// prints the value of the parameter on the user's display
	public String readConfig(String username, String password, String parameter)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;

	// sets the parameter to value
	public String setConfig(String username, String password, String parameter, String value)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException;
}
