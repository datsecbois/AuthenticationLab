import java.io.FileNotFoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class Servant extends UnicastRemoteObject implements Service {
	boolean isOn = false;

	public boolean isOn() {
		return isOn;
	}

	public Servant() throws RemoteException {
		super();
	}

	public String authenticate(String username, String password, String permission)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		return Auth.authenticate(username, password, permission);
	}

	public String print(String username, String password, String filename, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "print");
		if (message.equals(Auth.authenticatedMessage)) {
			if (!tasks.containsKey(printer)) {
				tasks.put(printer, new ArrayList<String>());
			}
			ArrayList<String> taskList = tasks.get(printer);
			taskList.add(filename);

			return "Added task " + filename + " on " + printer;
		} else {
			return message;
		}
	}

	public String queue(String username, String password, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "queue");
		if (message.equals(Auth.authenticatedMessage)) {
			String queue = "Queue (" + printer + "):";

			if (tasks.containsKey(printer)) {
				for (int i = 0; i < tasks.get(printer).size(); i++) {
					queue = queue + "\n    <" + (i + 1) + ">      <" + tasks.get(printer).get(i) + ">";
				}
			} else {
				queue = queue + "\n    No tasks";
			}

			return queue;
		} else {
			return message;
		}
	}

	public String topQueue(String username, String password, String printer, int job)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "topQueue");
		if (message.equals(Auth.authenticatedMessage)) {
			if (!tasks.containsKey(printer)) {
				return "Printer " + printer + " does not have any tasks queued";
			}

			ArrayList<String> printerTasks = tasks.get(printer);
			if (job >= printerTasks.size()) {
				return "Job no. " + job + " does not exist in " + printer + "'s task list";
			}

			ArrayList<String> newOrder = new ArrayList<>();
			newOrder.add(tasks.get(printer).get(job - 1));

			for (int i = 0; i < tasks.get(printer).size() - 1; i++) {
				if (i != job - 1) {
					newOrder.add(tasks.get(printer).get(i));
				}
			}

			tasks.remove(printer);
			tasks.put(printer, newOrder);
			return "Job no. " + job + " is moved to the top";
		} else {
			return message;
		}
	}

	public String start(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "start");
		if (message.equals(Auth.authenticatedMessage)) {
			isOn = true;
			return "Print server is started";
		} else {
			return message;
		}
	}

	public String stop(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "stop");
		if (message.equals(Auth.authenticatedMessage)) {
			isOn = false;
			return "Print server is stopped";
		} else {
			return message;
		}
	}

	public String restart(String username, String password)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "restart");
		if (message.equals(Auth.authenticatedMessage)) {
			stop(username, password);
			tasks.clear();
			isOn = true;
			return "Queue is cleared and print server has been restarted";
		} else {
			return message;
		}
	}

	public String status(String username, String password, String printer)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "status");
		if (message.equals(Auth.authenticatedMessage)) {
			String status = "";

			if (tasks.containsKey(printer)) {
				status = "Printing";
			} else {
				status = "Waiting for tasks";
			}

			return status;
		} else {
			return message;
		}
	}

	public String readConfig(String username, String password, String parameter)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "readConfig");
		if (message.equals(Auth.authenticatedMessage)) {
			if (config.containsKey(parameter)) {
				return "Value of parameter " + parameter + " is " + config.get(parameter);
			} else {
				return "Parameter " + parameter + " does not exist in the config";
			}
		} else {
			return message;
		}
	}

	public String setConfig(String username, String password, String parameter, String value)
			throws RemoteException, FileNotFoundException, NoSuchAlgorithmException {
		String message = authenticate(username, password, "setConfig");
		if (message.equals(Auth.authenticatedMessage)) {
			config.put(parameter, value);
			return "Parameter " + parameter + " is set to " + value;
		} else {
			return message;
		}
	}
}
