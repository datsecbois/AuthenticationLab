import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Auth {
	public static String authenticatedMessage = "authenticated and access allowed";

	public static String authenticate(String username, String password, String permission)
			throws FileNotFoundException, NoSuchAlgorithmException {
		boolean loginAuthenticated = false;
		boolean permissionAllowed = false;

		File passwd = new File("src\\file_system\\passwd.csv");
		File acl = new File("src\\file_system\\ACL.csv");
		// Non-windows path
		// File passwd = new File("file_system/passwd.csv");
		// File acl = new File("file_system/ACL.csv");

		Scanner passwdReader = new Scanner(passwd);
		Scanner aclReader = new Scanner(acl);

		while (passwdReader.hasNextLine()) {
			String passwdData = passwdReader.nextLine();
			String aclData = aclReader.nextLine();

			String[] credentials = passwdData.split(",");
			if (credentials[0].equals(username)) {
				String hash = hash(password, credentials[1]);
				passwdReader.close();
				loginAuthenticated = credentials[2].equals(hash);

				String[] ACL = aclData.split(",");
				// permissions:
				// [1]: print, [2]: queue, [3]: topQueue,
				// [4]: start, [5]: stop, [6]: restart,
				// [7]: status, [8]: readConfig, [9]: setConfig
				switch (permission) {
					case "print":
						permissionAllowed = Boolean.parseBoolean(ACL[1]);
						break;
					case "queue":
						permissionAllowed = Boolean.parseBoolean(ACL[2]);
						break;
					case "topQueue":
						permissionAllowed = Boolean.parseBoolean(ACL[3]);
						break;
					case "start":
						permissionAllowed = Boolean.parseBoolean(ACL[4]);
						break;
					case "stop":
						permissionAllowed = Boolean.parseBoolean(ACL[5]);
						break;
					case "restart":
						permissionAllowed = Boolean.parseBoolean(ACL[6]);
						break;
					case "status":
						permissionAllowed = Boolean.parseBoolean(ACL[7]);
						break;
					case "readConfig":
						permissionAllowed = Boolean.parseBoolean(ACL[8]);
						break;
					case "setConfig":
						permissionAllowed = Boolean.parseBoolean(ACL[9]);
						break;
					default:
						permissionAllowed = false;
				}
				aclReader.close();
				if (loginAuthenticated && permissionAllowed) {
					return authenticatedMessage;
				} else if (loginAuthenticated && !permissionAllowed) {
					return "user has not permission to this operation";
				} else {
					return "User could not be authenticated";
				}
			}
		}

		passwdReader.close();
		aclReader.close();
		return "User could not be authenticated";
	}

	public static String hash(String password, String salt) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		String input = password + salt;
		byte[] bytes = md.digest(input.getBytes(StandardCharsets.UTF_8));
		return String.format("%064x", new BigInteger(1, bytes));
	}

	private static String[] users = { "alice", /* "bob", */ "cecilia", "david", "erica", "fred",
			"george", "henry", "ida" };
	private static String[] passwords = { "1234", /* "password", */ "Hippopotamus7Androgynous2",
			"4321", "winter2022", "kodeord", "qwerty", "asdfghjkl", "zaqwsx" };
	private static String[] functions = { "print", "queue", "topQueue", "start", "stop", "restart", "status",
			"readConfig",
			"setConfig" };

	public static void testAccess() throws FileNotFoundException, NoSuchAlgorithmException {
		for (int i = 0; i < users.length; i++) {
			System.out.println("\n\n" + users[i] + " requesting...");
			for (String func : functions) {
				System.out
						.print("\t" + func + ": " + authenticate(users[i], passwords[i], func).equals(authenticatedMessage));
			}
		}
	}

	public static void main(String[] args) throws NoSuchAlgorithmException, FileNotFoundException {
		testAccess();
	}

	private static String[] salts = { "aiGh9ohwoh1eeBee", /* "ooNoo0huph3rohTh", */ "bahL5vah6wi5UCho",
			"aiphait7johVa6ah",
			"ooc8Eequ6oomah3D", "jee3aiy7nuijah1E", "zeFahpie3Tae2eec", "ohg8chooSaong3sh", "theashie0phoo0Yu" };

	public static void generateHashes() throws NoSuchAlgorithmException {
		for (int i = 0; i < users.length; i++) {
			System.out.println(users[i] + ": " + passwords[i] + " + " + salts[i] + " = " + hash(passwords[i], salts[i]));
		}
	}
}
